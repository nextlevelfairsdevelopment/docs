\
 {.western}
=

[User Side API](#h.bns0cvcutnsm)

[Unsigned Requests](#h.eyfvgkgm8sde)

[User Registration](#h.8um5rwpocfbr)

[User Login](#h.evv4rcmn0w1f)

[Signed Requests](#h.2qn3oss9ly0b)

[Updating Profile](#h.14y23qvo4t8)

[Update User Location](#h.13q7q0v9vhap)

[Updating Thing](#h.1ibval99igou)

[Adding Payment Token](#h.v41e7zbkb4qo)

[Removing a Payment Token](#h.fkbelh82nmsm)

[Retrieve all cards](#h.ftxblpe3b4f6)

[Creating a referral code](#h.3n83aoo9rjyw)

[Retrieving Referral Code & Credits](#h.ftt16lzcy1kz)

[Applying referral code](#h.gnehd9yaqqf2)

[Get available Providers](#h.648xd0f3vpnd)

[Select Provider](#h.gm5paxpv9co)

[Cancel Request](#h.bvftzq8s4tys)

[Creating a Service Request](#h.m8yrw8f7yq0x)

[Check Request](#h.ylbjk0sxyyqk)

[Ongoing Request](#h.9vcryizh2ca6)

[Cancel a Request](#h.xcvkasqs9mw0)

[Path of Request](#h.4kacoik3w32)

[View Request History](#h.fxpnqdaevi2k)

[Reviewing a Provider](#h.ih94dfiqugjx)

[Provider Side API](#h.2zm00nqrko2i)

[Unsigned Requests](#h.6i3nevuczc0n)

[Login](#h.nrvpu07ts2l0)

[Signed Requests](#h.1dnzs0iaxetc)

[Updating Profile](#h.yyx14dh5rnen)

[Update Provider Services](#h.2q3eyxywnqd7)

[Get Provider Services](#h.4mp0ofhszusi)

[Update Provider Location](#h.vr5s5kvjnro5)

[Toggle Provider’s State](#h.7edjao8r2en6)

[Check Provider’s State](#h.b5o44im14eub)

[Retrieve all active service requests](#h.erntsrb9cic2)

[Check Request](#h.kngbjbff472m)

[Ongoing Request](#h.y4i7u0ldiqez)

[Path of Request](#h.erui4i2ywihl)

[View Request History](#h.2x28kzqk05pv)

[Responding to a Request](#h.kyxr7vbznqgh)

[Provider Started](#h.9avdwfv8mngw)

[Provider Arrived](#h.w8fnspreeksf)

[Service Started](#h.5zhl9svav6wo)

[Update Service Location](#h.nwvb5rmyuvdv)

[Service Completed](#h.pe4wwpihy6xj)

[Reviewing a User](#h.jcp6oy2mzmf)

[Common API’s](#h.9zjwnsd8jkvc)

[Getting the list of all Information Pages](#h.eb0au2ct3mt5)

[Getting a particular Information Page](#h.xguawgapr12p)

[Getting the list of all Types](#h.l3du7p7dvatb)

[Forgot Password](#h.lawziyogd8yh)

\

\

\

\

\

\

\

\

\

\

\

\

\

\

\

\

\

\

\

\

\

\

\

\

\

\

\

\

\

\

\

\

\

\

\

\

User Side API {.western}
=============

\

Unsigned Requests {.western}
-----------------

### User Registration {.western}

You can create a new user either by passing password or social unique
ID.

\

\

**API Endpoint**

**/owner/register**

\

**Request Method**

**POST**

\

**Parameters**

\

  -------------- ------------------------ ------------ -------------------------------------------------
  **Required**   **Parameters**           **Format**   **Note**

  **X**          **email**                **string**   \
                                                       

  **X**          **password**             **string**   **required if login\_by is manual**

  **X**          **first\_name**          **string**   \
                                                       

  **X**          **last\_name**           **string**   \
                                                       

  **X**          **phone**                **string**   \
                                                       

  **X**          **picture**              **binary**   **type should jpeg, bmp or png**

  **X**          **device\_token**        **string**   \
                                                       

  **X**          **device\_type**         **string**   **android, ios**

  \              **bio**                  **string**   \
                                                       

  \              **zipcode**              **string**   \
                                                       

  \              **address**              **string**   \
                                                       

  \              **state**                **string**   \
                                                       

  \              **country**              **string**   \
                                                       

  **X**          **login\_by**            **string**   **manual, facebook, google**

  **X**          **social\_unique\_id**   **string**   **required if login\_by is facebook or google**
  -------------- ------------------------ ------------ -------------------------------------------------

\

\

**Example Response**

\

{

"success":true,

"id":8,

"first\_name":"vinoth",

"last\_name":"kumar",

"phone":"9894505555",

"email":"[praba@gmail.com](mailto:praba@gmail.com)",

"picture":"https:\\/\\/wag-prabakaran.s3.amazonaws.com\\/d03c1619389efdcc13fdd5b6cd4811471575c91c",

"bio":"i'm a bad man",

"address":"dubai, dubai main road",

"state":"dubai",

"country":"uae",

"zipcode":"111111",

"login\_by":”manual”,

"social\_unique\_id":null,

"device\_token":"asdasdasdasd",

"device\_type":"ios",

"token":"2y10CfQIZGbv0dtFfzGls4FQtOzbqJC3E0IAORQvkUdoeuY5R6gJjZvpm"

}

\

\

### \
 {.western}

### User Login {.western}

By default token will be used to access the signed requests. If you lost
your token at client side or if your token has been expired you can use
this endpoint to get back the token.

\

**API Endpoint**

**/owner/login**

\

**Request Method**

**POST**

\

**Parameters**

\

  -------------- ------------------------ ------------- -------------------------------------------------
  **Required**   **Parameters**           **Format**    **Explanation**

  **X**          **email**                **string**    **required if login\_by is manual**

  **X**          **password**             **string**    **required if login\_by is manual**

  **X**          **login\_by**            **integer**   **manual, facebook, google**

  **X**          **device\_token**        **string**    \
                                                        

  **X**          **device\_type**         **string**    **android,ios**

  **X**          **social\_unique\_id**   **string**    **required if login\_by is facebook or google**
  -------------- ------------------------ ------------- -------------------------------------------------

\

\

\

\

\

\

\

\

\

\

\

**Example Response**

\

{

"success":true,

"id":8,

"first\_name":"vinoth",

"last\_name":"kumar",

"phone":"9894505555",

"email":"[praba@gmail.com](mailto:praba@gmail.com)",

"picture":"https:\\/\\/wag-prabakaran.s3.amazonaws.com\\/d03c1619389efdcc13fdd5b6cd4811471575c91c",

"bio":"i'm a bad man",

"address":"dubai, dubai main road",

"state":"dubai",

"country":"uae",

"zipcode":"111111",

"login\_by":”manual”,

"social\_unique\_id":null,

"device\_token":"asdasdasdasd",

"device\_type":"ios",

"token":"2y10CfQIZGbv0dtFfzGls4FQtOzbqJC3E0IAORQvkUdoeuY5R6gJjZvpm"

}

\

\

\

\

\

\

\

\

\

\

\

\

\

\

\

\

\

\

\

\

\

\

\

\

\

\

Signed Requests {.western}
---------------

For all signed requests following parameters are mandatory and has to
passed along with the other request parameters.

\

  -------------- ---------------- ------------ -----------------
  **Required**   **Parameters**   **Format**   **Explanation**

  **X**          **id**           **int**      **User ID**

  **X**          **token**        **string**   \
                                               
  -------------- ---------------- ------------ -----------------

\

\

\

\

### Updating Profile {.western style="line-height: 100%; page-break-inside: avoid; page-break-after: avoid"}

You call update the user profile by passing only the fields that needs
to be updated.

\

**API Endpoint**

**/user/update**

\

**Request Method**

**POST**

\

**Parameters**

\

  -------------- ----------------- ------------ ----------------------------------
  **Required**   **Parameters**    **Format**   **Explanation**

  \              **first\_name**   **string**   \
                                                

  \              **last\_name**    **string**   \
                                                

  \              **email**         **string**   \
                                                

  \              **phone**         **string**   \
                                                

  \              **password**      **string**   \
                                                

  \              **picture**       **File**     **type should jpeg, bmp or png**
                                                

  \              **bio**           **string**   \
                                                

  \              **address**       **string**   \
                                                

  \              **state**         **string**   \
                                                

  \              **country**       **string**   \
                                                

  \              **zipcode**       **string**   \
                                                
  -------------- ----------------- ------------ ----------------------------------

\

**Example Response**

\

{

"success":true,

}

\

### Update User Location {.western style="line-height: 100%; page-break-inside: avoid; page-break-after: avoid"}

Updates the user’s current location.

\

**API Endpoint**

**/user/location**

\

**Request Method**

**POST**

\

**Parameters**

\

  -------------- ---------------- ------------ -----------------
  **Required**   **Parameters**   **Format**   **Explanation**

  **X**          **latitude**     **float**    \
                                               

  **X**          **longitude**    **float**    \
                                               
  -------------- ---------------- ------------ -----------------

\

\

\

**Example Response**

\

{

"success":true,

}

\

\

### Updating Thing {.western style="line-height: 100%; page-break-inside: avoid; page-break-after: avoid"}

Creates a thing if its not present. Else thing will get updated.

\

**API Endpoint**

**/user/updatething**

\

**Request Method**

**POST**

\

**Parameters**

\

  -------------- ---------------- ------------ ----------------------------------
  **Required**   **Parameters**   **Format**   **Explanation**

  \              **name**         **string**   \
                                               

  \              **age**          **number**   \
                                               

  \              **type**         **string**   \
                                               

  \              **notes**        **string**   \
                                               

  \              **picture**      **file**     **type should jpeg, bmp or png**
                                               
  -------------- ---------------- ------------ ----------------------------------

\

**Example Response**

\

{

"success":true,

}

\

\

\

\

\

\

\

### Adding Payment Token {.western style="line-height: 100%; page-break-inside: avoid; page-break-after: avoid"}

Generate the Customer ID from stripe by passing the stripe token. This
Customer ID will be used to charge the user.

\

**API Endpoint**

**api/payment/addCardToken**

\

**Request Method**

**POST**

\

**Parameters**

\

  -------------- -------------------- ------------ -------------------------------------
  **Required**   **Parameters**       **Format**   **Explanation**
  **X**          **payment\_token**   **string**   **Stripe Customer ID**
  **X**          **last\_four**       **number**   **last four digits of credit card**
  -------------- -------------------- ------------ -------------------------------------

\

**Example Response**

\

{

"success":true,

}

\

### Removing a Payment Token {.western}

\

Removes the card associated with passed card\_id.

\

**API Endpoint**

**api/payment/deleteCardToken**

\

**Request Method**

**POST**

\

\

\

\

\

\

\

**Parameters**

\

  -------------- ---------------- ------------ -----------------
  **Required**   **Parameters**   **Format**   **Explanation**

  **X**          **card\_id**     **int**      \
                                               
  -------------- ---------------- ------------ -----------------

\

**Example Response**

\

{

"success":true,

}

\

### \
 {.western style="line-height: 100%; page-break-inside: avoid; page-break-after: avoid"}

### Retrieve all cards {.western style="line-height: 100%; page-break-inside: avoid; page-break-after: avoid"}

\

**API Endpoint**

**/user/cards**

\

**Request Method**

**POST**

\

**Parameters**

****No Parameters Required.

\

\

**Example Response**

\

{

"success": true,

"payments": [

{

"id": "1",

"customer\_id": "dsknisin",

"created\_at": "2014-10-21 01:51:02",

"updated\_at": "2014-10-21 01:51:02",

"owner\_id": "1",

“last\_four” : “4563”,

}

]

}

\

\

\

### Creating a referral code {.western style="line-height: 100%; page-break-inside: avoid; page-break-after: avoid"}

Each user can create a referral code which they can share with their
friends to earn credits if the user signs up.

\

**API Endpoint**

**/user/referral**

\

**Request Method**

**POST**

\

**Parameters**

\

  -------------- ---------------- ------------ -----------------
  **Required**   **Parameters**   **Format**   **Explanation**

  **X**          **code**         **string**   \
                                               
  -------------- ---------------- ------------ -----------------

\

**Example Response**

\

{

"success":true,

}

\

\

\

### Retrieving Referral Code & Credits {.western style="line-height: 100%; page-break-inside: avoid; page-break-after: avoid"}

Retrieves the corresponding user’s referral code, the credits he has
earned and total no of people he has referred.

\

**API Endpoint**

**/user/referral**

\

**Request Method**

**GET**

\

**Parameters**

No parameters required.

\

**Example Response**

\

{

success: true,

referral\_code: "PRABHA",

total\_referrals: "1",

amount\_earned: "10.00",

amount\_spent: "0.00",

balance\_amount: 10

}

\

### Applying referral code {.western style="line-height: 100%; page-break-inside: avoid; page-break-after: avoid"}

Referral code can be applied by any user during signup. So the person
who has referred him will get credits.

\

**API Endpoint**

**/user/apply-referral**

\

**Request Method**

**POST**

\

**Parameters**

\

  -------------- -------------------- ------------ -----------------
  **Required**   **Parameters**       **Format**   **Explanation**

  **X**          **referral\_code**   **string**   \
                                                   
  -------------- -------------------- ------------ -----------------

\

\

\

**Example Response**

\

{

"success":true,

}

\

\

### Get available Providers {.western style="line-height: 100%; page-break-inside: avoid; page-break-after: avoid"}

Retrieves all available providers given the request parameters. Check if
provider\_selection setting is 2. Can be set from admin panel, settings
tab.

\

**API Endpoint**

**/user/getproviders**

\

**Request Method**

**POST**

\

**Parameters**

\

  -------------- ---------------- ------------ -----------------------------------------------------------
  **Required**   **Parameters**   **Format**   **Explanation**

  **x**          **longitude**    **float**    \
                                               

  **x**          **longitude**    **float**    \
                                               

  **x**          **distance**     **int**      **The distance radius in which the providers should be.**

  **x**          **type**         **array**    **All the service type id as array.**
  -------------- ---------------- ------------ -----------------------------------------------------------

\

\

**Example Response**

\

**{**

**"success":****true****,**

**"request\_id":****44****,**

**"walkers": [**

**{**

**"id":****"1"****,**

**"distance":****"1.0712398612872915"**

**}**

**]**

**}**

\

### Select Provider {.western style="line-height: 100%; page-break-inside: avoid; page-break-after: avoid"}

Select a provider from the list given. Check if provider\_selection
setting is 2. Can be set from admin panel, settings tab.

\

**API Endpoint**

**/user/createrequestproviders**

\

**Request Method**

**POST**

\

**Parameters**

\

  -------------- ------------------ ------------ -----------------
  **Required**   **Parameters**     **Format**   **Explanation**

  **x**          **request\_id**    **int**      \
                                                 

  **x**          **provider\_id**   **int**      \
                                                 
  -------------- ------------------ ------------ -----------------

\

\

**Example Response**

\

**{**

**"success":****true****,**

**"request\_id":****"44"**

**}**

\

### Cancel Request {.western style="line-height: 100%; page-break-inside: avoid; page-break-after: avoid"}

Cancels a request during display of providers or after selecting
provider. Check if provider\_selection setting is 2. Can be set from
admin panel, settings tab.

\

**API Endpoint**

**/user/cancellation**

\

**Request Method**

**POST**

\

**Parameters**

\

  -------------- ----------------- ------------ -----------------
  **Required**   **Parameters**    **Format**   **Explanation**

  **x**          **request\_id**   **int**      \
                                                
  -------------- ----------------- ------------ -----------------

\

\

**Example Response**

\

**{**

**"success":****true****,**

**"deleted request\_id":****"44"**

**}**

\

\

### Creating a Service Request {.western}

Creates a service request. So, the providers nearby will get request
notification which they can accept and complete it. Check if
provider\_selection setting is 1. Can be set from admin panel, settings
tab.

\

**API Endpoint**

**/user/createrequest**

\

**Request Method**

**POST**

\

**Parameters**

\

  -------------- ---------------- ------------ -----------------------------------------------------------
  **Required**   **Parameters**   **Format**   **Explanation**

  **x**          **longitude**    **float**    \
                                               

  **x**          **longitude**    **float**    \
                                               

  **x**          **distance**     **int**      **The distance radius in which the providers should be.**

  \              **type**         **int**      \
                                               
  -------------- ---------------- ------------ -----------------------------------------------------------

\

\

**Example Response**

\

**{**

**"success":true,**

**}**

\

\

### Check Request {.western style="line-height: 100%; page-break-inside: avoid; page-break-after: avoid"}

It gives all the data corresponding to a particular request such as
provider profile, Bill profile, request status etc.

\

**API Endpoint**

**/user/getrequest**

\

**Request Method**

**GET**

\

**Parameters**

\

  -------------- ----------------- ------------ -----------------
  **Required**   **Parameters**    **Format**   **Explanation**

  **x**          **request\_id**   **int**      \
                                                
  -------------- ----------------- ------------ -----------------

\

**Example Response**

  ----------------------------------------------------------------------------------------------------
  {
  success: true,
  status: "1",
  is\_walker\_started: "1",
  is\_walker\_arrived: "1",
  is\_walk\_started: "1",
  is\_completed: "1",
  is\_walker\_rated: "1",
  walker: {
  first\_name: "vijay",
  last\_name: "kanth",
  phone: "9894505555",
  bio: "I'm a mokka man",
  picture: "https:\\/\\/wag-prabakaran.s3.amazonaws.com\\/4d6d934a35f72fd8dfb2c6b02ebc2af3aeead369",
  latitude: "11.35",
  longitude: "76.94"
  },
  bill: {
  distance: "5.00",
  time: "8.00",
  base\_price: "50.00",
  distance\_cost: "50.00",
  time\_cost: "64.00",
  total: "164.00",
  is\_paid: "0"
  }
  }
  ----------------------------------------------------------------------------------------------------

\

\

\

### Ongoing Request {.western style="line-height: 100%; page-break-inside: avoid; page-break-after: avoid"}

Returns the id of the ongoing request.

\

**API Endpoint**

**/user/requestinprogress**

\

**Request Method**

**GET**

\

**Parameters**

****No Parameters required

\

**Example Response**

{

request\_id: "1",

success:true,

}

\

\

### Cancel a Request {.western style="line-height: 100%; page-break-inside: avoid; page-break-after: avoid"}

Corresponding request will be cancelled if the provider has not
completed the request.

\

**API Endpoint**

**/user/cancelrequest**

\

**Request Method**

**POST**

\

**Parameters**

\

  -------------- ----------------- ------------ -----------------
  **Required**   **Parameters**    **Format**   **Explanation**

  **X**          **request\_id**   **int**      \
                                                
  -------------- ----------------- ------------ -----------------

\

\

\

**Example Response**

\

**{**

**"success":true,**

**}**

\

\

### Path of Request {.western style="line-height: 100%; page-break-inside: avoid; page-break-after: avoid"}

It returns the list of latitudes and longitudes of a particular request
with the timestamps for each location.

\

**API Endpoint**

**/user/requestpath**

\

**Request Method**

**GET**

\

**Parameters**

\

  -------------- ----------------- --------------- ---------------------
  **Required**   **Parameters**    **Format**      **Explanation**

  **X**          **request\_id**   **int**         \
                                                   

  \              **ts**            **Date Time**   Sample Input
                                                   
                                                   2014-11-17 05:59:40
  -------------- ----------------- --------------- ---------------------

\

**Example Response**

\

{

"success": true,

"locationdata": [

{

"latitude": "1.00000000",

"longitude": "1.00000000",

"distance": "0.000",

"timestamp": {

"date": "2014-11-17 13:53:26",

"timezone\_type": 3,

"timezone": "UTC"

}

},

{

"latitude": "1.00000000",

"longitude": "1.00000000",

"distance": "0.000",

"timestamp": {

"date": "2014-11-17 13:53:43",

"timezone\_type": 3,

"timezone": "UTC"

}

},

]

}

\

\

### View Request History {.western style="line-height: 100%; page-break-inside: avoid; page-break-after: avoid"}

Return the list of all previous requests of a particular users.

\

\

**API Endpoint**

**/user/history**

\

**Request Method**

**GET**

\

**Parameters**

No Parameters required

\

\

**Example Response**

\

{

"success": true,

"requests": [

{

"id": "11",

"date": "2014-11-10 07:24:13",

"distance": "0.00000000",

"time": "0.00",

"base\_price": "0.00",

"distance\_cost": "0.00",

"time\_cost": "0.00",

"total": "0.00",

"type": "Benz",

"type\_icon":
"https:\\/\\/wag-prabakaran.s3.amazonaws.com\\/14158620471615367883",

"walker": {

"first\_name": "test",

"last\_name": "test last",

"phone": "7708288018",

"email": "test@gmail.com",

"picture": "https:\\/\\/wag-prabakaran.s3.amazonaws.com\\/b81",

"bio": "tes"

}

}

]

}

\

### Reviewing a Provider {.western style="line-height: 100%; page-break-inside: avoid; page-break-after: avoid"}

Posting a rating and review for a request.

\

**API Endpoint**

**/user/rating**

\

**Request Method**

**POST**

\

**Parameters**

\

  -------------- ----------------- ------------ -----------------
  **Required**   **Parameters**    **Format**   **Explanation**

  **X**          **request\_id**   **int**      \
                                                

  **X**          **rating**        **int**      \
                                                

  **X**          **comment**       **string**   \
                                                
  -------------- ----------------- ------------ -----------------

\

**Example Response**

\

{

"success":true,

}

\

\

\

\

\

Provider Side API {.western style="line-height: 100%; page-break-inside: avoid; page-break-after: avoid"}
=================

\

Unsigned Requests {.western style="line-height: 100%; page-break-inside: avoid; page-break-after: avoid"}
-----------------

### Login {.western style="line-height: 100%; page-break-inside: avoid; page-break-after: avoid"}

By default token will be used to access the signed requests. If you lost
your token at client side or if your token has been expired you can use
this endpoint to get back the token.

\

**API Endpoint**

**/owner/login**

\

**Request Method**

**POST**

\

**Parameters**

\

  -------------- ------------------------ ------------- -------------------------------------------------
  **Required**   **Parameters**           **Format**    **Explanation**

  **X**          **email**                **string**    **required if login\_by is manual**

  **X**          **password**             **string**    **required if login\_by is manual**

  **X**          **login\_by**            **integer**   **manual, facebook, google**

  **X**          **device\_token**        **string**    \
                                                        

  **X**          **device\_type**         **string**    **android,ios**

  **X**          **social\_unique\_id**   **string**    **required if login\_by is facebook or google**
  -------------- ------------------------ ------------- -------------------------------------------------

\

\

\

\

\

\

\

\

\

\

\

**Example Response**

\

{

"success":true,

"id":8,

"first\_name":"vinoth",

"last\_name":"kumar",

"phone":"9894505555",

"email":"[praba@gmail.com](mailto:praba@gmail.com)",

"picture":"https:\\/\\/wag-prabakaran.s3.amazonaws.com\\/d03c1619389efdcc13fdd5b6cd4811471575c91c",

"bio":"i'm a bad man",

"address":"dubai, dubai main road",

"state":"dubai",

"country":"uae",

"zipcode":"111111",

"login\_by":”manual”,

"social\_unique\_id":null,

"device\_token":"asdasdasdasd",

"device\_type":"ios",

"token":"2y10CfQIZGbv0dtFfzGls4FQtOzbqJC3E0IAORQvkUdoeuY5R6gJjZvpm",

“type”:0

}

\

\

Signed Requests {.western}
---------------

\

### Updating Profile {.western style="line-height: 100%; page-break-inside: avoid; page-break-after: avoid"}

You call update the user profile by passing only the fields that needs
to be updated.

\

**API Endpoint**

**/provider/update**

\

**Request Method**

**POST**

\

**Parameters**

\

  -------------- ----------------- ------------ ----------------------------------
  **Required**   **Parameters**    **Format**   **Explanation**

  \              **first\_name**   **string**   \
                                                

  \              **last\_name**    **string**   \
                                                

  \              **email**         **string**   \
                                                

  \              **phone**         **string**   \
                                                

  \              **password**      **string**   \
                                                

  \              **picture**       **File**     **type should jpeg, bmp or png**
                                                

  \              **bio**           **string**   \
                                                

  \              **address**       **string**   \
                                                

  \              **state**         **string**   \
                                                

  \              **country**       **string**   \
                                                

  \              **zipcode**       **string**   \
                                                
  -------------- ----------------- ------------ ----------------------------------

\

**Example Response**

\

{

"success":true,

}

\

### Update Provider Services {.western style="line-height: 100%; page-break-inside: avoid; page-break-after: avoid"}

Updates the provider’s services and quotes for each service.

\

**API Endpoint**

**/provider\_services/update**

\

**Request Method**

**POST**

\

**Parameters**

\

  -------------- ------------------------------ ------------ ---------------------
  **Required**   **Parameters**                 **Format**   **Explanation**

  **X**          **service**                    **array**    **service type id**

  **X**          **service\_base\_price**       **array**    \
                                                             

  **X**          **service\_price\_distance**   **array**    \
                                                             

  **X**          **service\_price\_time**       **array**    \
                                                             
  -------------- ------------------------------ ------------ ---------------------

\

**Example Response**

\

{

"success":true,

}

\

### Get Provider Services {.western style="line-height: 100%; page-break-inside: avoid; page-break-after: avoid"}

Updates the provider’s current location.

\

**API Endpoint**

**/provider/services\_details**

\

**Request Method**

**GET**

\

**Example Response**

**{**

**"success":****true****,**

**"serv\_name": [**

**"Waxing"**

**],**

**"serv\_base\_price": [**

**"10.00"**

**],**

**"serv\_per\_distance": [**

**"15.00"**

**],**

**"serv\_per\_time": [**

**"20.00"**

**]**

**}**

\

### Update Provider Location {.western style="line-height: 100%; page-break-inside: avoid; page-break-after: avoid"}

Updates the provider’s current location.

\

**API Endpoint**

**/provider/location**

\

**Request Method**

**POST**

\

**Parameters**

\

  -------------- ---------------- ------------ -----------------
  **Required**   **Parameters**   **Format**   **Explanation**

  **X**          **latitude**     **float**    \
                                               

  **X**          **longitude**    **float**    \
                                               
  -------------- ---------------- ------------ -----------------

\

**Example Response**

\

{

"success":true,

}

\

### Toggle Provider’s State {.western style="line-height: 100%; page-break-inside: avoid; page-break-after: avoid"}

Toggles the availability of the provider.

\

**API Endpoint**

**/provider/togglestate**

\

**Request Method**

**POST**

\

**Parameters**

****No Parameters required.

\

**Example Response**

\

{

“is\_active”:0,

"success":true,

}

\

### Check Provider’s State {.western style="line-height: 100%; page-break-inside: avoid; page-break-after: avoid"}

Retrieves the availability of the provider. If is\_active is 1 then, the
provider is available.

\

**API Endpoint**

**/provider/checkstate**

\

**Request Method**

**GET**

\

**Parameters**

****No Parameters required.

\

**Example Response**

\

{

“is\_active”:0,

"success":true,

}

\

### Retrieve all active service requests {.western style="line-height: 100%; page-break-inside: avoid; page-break-after: avoid"}

Retrieves all the unresponded service requests.

\

**API Endpoint**

**/provider/getrequests**

\

**Request Method**

**GET**

\

**Parameters**

****No Parameters required.

\

**Example Response**

\

{

success: true,

incoming\_requests: [

{

request\_id: "2",

time\_left\_to\_respond: "53"

},

]

}

### Check Request {.western style="line-height: 100%; page-break-inside: avoid; page-break-after: avoid"}

It gives all the data corresponding to a particular request such as
provider profile, Bill profile, request status etc.

\

**API Endpoint**

**/provider/getrequest**

\

**Request Method**

**GET**

\

**Parameters**

\

  -------------- ----------------- ------------ -----------------
  **Required**   **Parameters**    **Format**   **Explanation**

  **x**          **request\_id**   **int**      \
                                                
  -------------- ----------------- ------------ -----------------

\

**Example Response**

{

success: true,

request: {

is\_walker\_started: "0",

is\_walker\_arrived: "0",

is\_started: "0",

is\_completed: "0",

is\_dog\_rated: "1",

owner: {

name: "vinoth kumar",

picture:
"https:\\/\\/wag-prabakaran.s3.amazonaws.com\\/55761d8b50048b5d32217ff9d3558bc3233b2971",

phone: "9894505555",

address: "dubai, dubai main road,",

latitude: "0.00000000",

longitude: "0.00000000"

},

\

dog: {

name: "Honda Civic",

age: "7",

breed: "New",

likes: "Blah Blah",

picture:
"https:\\/\\/wag-prabakaran.s3.amazonaws.com\\/14138561961967689435"

},

bill: {

distance: "5.00",

time: "8.00",

base\_price: "50.00",

distance\_cost: "50.00",

time\_cost: "64.00",

total: "164.00",

is\_paid: "0"

}

}

}

\

### \
 {.western style="line-height: 100%; page-break-inside: avoid; page-break-after: avoid"}

### \
 {.western style="line-height: 100%; page-break-inside: avoid; page-break-after: avoid"}

### Ongoing Request {.western style="line-height: 100%; page-break-inside: avoid; page-break-after: avoid"}

Returns the id of the ongoing request.

\

**API Endpoint**

**/provider/requestinprogress**

\

**Request Method**

**GET**

\

**Parameters**

****No Parameters required

\

**Example Response**

{

request\_id: "1",

success:true,

}

\

\

\

### Path of Request {.western style="line-height: 100%; page-break-inside: avoid; page-break-after: avoid"}

It returns the list of latitudes and longitudes of a particular request
with the timestamps for each location.

\

**API Endpoint**

**/provider/requestpath**

\

**Request Method**

**GET**

\

**Parameters**

\

  -------------- ----------------- --------------- ---------------------
  **Required**   **Parameters**    **Format**      **Explanation**

  **X**          **request\_id**   **int**         \
                                                   

  \              **ts**            **Date Time**   Sample Input
                                                   
                                                   2014-11-17 05:59:40
  -------------- ----------------- --------------- ---------------------

\

**Example Response**

\

{

"success": true,

"locationdata": [

{

"latitude": "1.00000000",

"longitude": "1.00000000",

"distance": "0.000",

"timestamp": {

"date": "2014-11-17 13:53:26",

"timezone\_type": 3,

"timezone": "UTC"

}

},

{

"latitude": "1.00000000",

"longitude": "1.00000000",

"distance": "0.000",

"timestamp": {

"date": "2014-11-17 13:53:43",

"timezone\_type": 3,

"timezone": "UTC"

}

},

]

}

\

\

### View Request History {.western style="line-height: 100%; page-break-inside: avoid; page-break-after: avoid"}

Return the list of all previous requests of a particular users.

\

\

**API Endpoint**

**/provider/history**

\

**Request Method**

**GET**

\

**Parameters**

No Parameters required

\

\

**Example Response**

\

{

"success": true,

"requests": [

{

"id": "11",

"date": "2014-11-10 07:24:13",

"distance": "0.00000000",

"time": "0.00",

"base\_price": "0.00",

"distance\_cost": "0.00",

"time\_cost": "0.00",

"total": "0.00",

"owner": {

"first\_name": "praba",

"last\_name": "karan",

"phone": "9894505555",

"email": "praba@gmail.com",

"picture":
"https:\\/\\/wag-prabakaran.s3.amazonaws.com\\/38644aa2d374db30e7cbbbe12aea641954fc9809",

"bio": "i'm a bad man"

}

}

]

}

\

\

### Responding to a Request {.western style="line-height: 100%; page-break-inside: avoid; page-break-after: avoid"}

If accepted is set to 1 then provider is assigned to corresponding
request. Else request switches to next available provider.

\

**API Endpoint**

**/provider/respondrequest**

\

**Request Method**

**POST**

\

**Parameters**

\

  -------------- ----------------- ------------ -----------------
  **Required**   **Parameters**    **Format**   **Explanation**

  **X**          **request\_id**   **int**      \
                                                

  **X**          **accepted**      **int**      \
                                                
  -------------- ----------------- ------------ -----------------

\

**Example Response**

\

{

"success":true,

}

\

\

\

### Provider Started {.western style="line-height: 100%; page-break-inside: avoid; page-break-after: avoid"}

This has to be called when provider starts from his place.

\

**API Endpoint**

**/provider/requestwalkerstarted******

\

**Request Method**

**POST**

\

**Parameters**

\

  -------------- ----------------- ------------ -----------------
  **Required**   **Parameters**    **Format**   **Explanation**

  **X**          **request\_id**   **int**      \
                                                

  **X**          **latitude**      **float**    \
                                                

  **X**          **longitude**     **float**    \
                                                
  -------------- ----------------- ------------ -----------------

\

**Example Response**

\

{

"success":true,

}

\

### Provider Arrived {.western style="line-height: 100%; page-break-inside: avoid; page-break-after: avoid"}

This has to be called when provider reaches user’s place.

\

**API Endpoint**

**/provider/requestwalkerarrived**

\

**Request Method**

**POST**

\

**Parameters**

\

  -------------- ----------------- ------------ -----------------
  **Required**   **Parameters**    **Format**   **Explanation**

  **X**          **request\_id**   **int**      \
                                                

  **X**          **latitude**      **float**    \
                                                

  **X**          **longitude**     **float**    \
                                                
  -------------- ----------------- ------------ -----------------

\

**Example Response**

\

{

"success":true,

}

\

\

\

### Service Started {.western style="line-height: 100%; page-break-inside: avoid; page-break-after: avoid"}

This has to be called when provider reaches user’s place.

\

**API Endpoint**

**/provider/requestwalkstarted**

\

**Request Method**

**POST**

\

**Parameters**

\

  -------------- ----------------- ------------ -----------------
  **Required**   **Parameters**    **Format**   **Explanation**

  **X**          **request\_id**   **int**      \
                                                

  **X**          **latitude**      **float**    \
                                                

  **X**          **longitude**     **float**    \
                                                
  -------------- ----------------- ------------ -----------------

\

**Example Response**

\

{

"success":true,

}

\

\

### Update Service Location {.western style="line-height: 100%; page-break-inside: avoid; page-break-after: avoid"}

This has to be called to update the location of the service.

\

**API Endpoint**

**/request/location**

\

**Request Method**

**POST**

\

**Parameters**

\

  -------------- ----------------- ------------ -----------------
  **Required**   **Parameters**    **Format**   **Explanation**

  **X**          **request\_id**   **int**      \
                                                

  **X**          **latitude**      **float**    \
                                                

  **X**          **longitude**     **float**    \
                                                
  -------------- ----------------- ------------ -----------------

\

**Example Response**

\

{

"success":true,

}

\

\

### Service Completed {.western style="line-height: 100%; page-break-inside: avoid; page-break-after: avoid"}

This has to be called when provider reaches user’s place.

\

**API Endpoint**

**/provider/requestwalkercompleted**

\

**Request Method**

**POST**

\

**Parameters**

\

  -------------- ----------------- ------------ -----------------
  **Required**   **Parameters**    **Format**   **Explanation**

  **X**          **request\_id**   **int**      \
                                                

  **X**          **latitude**      **float**    \
                                                

  **X**          **longitude**     **float**    \
                                                

  **X**          **distance**      **float**    \
                                                

  **X**          **time**          **float**    \
                                                
  -------------- ----------------- ------------ -----------------

\

**Example Response**

\

{

success: true,

base\_fare: "50.00",

distance\_cost: 50,

time\_cost: 64,

total: 164

is\_paid : 1

}

\

\

\

### Reviewing a User {.western style="line-height: 100%; page-break-inside: avoid; page-break-after: avoid"}

Posting a rating and review for a request.

\

**API Endpoint**

**/provider/rating**

\

**Request Method**

**POST**

\

**Parameters**

\

  -------------- ----------------- ------------ -----------------
  **Required**   **Parameters**    **Format**   **Explanation**

  **X**          **request\_id**   **int**      \
                                                

  **X**          **rating**        **int**      \
                                                

  **X**          **comment**       **string**   \
                                                
  -------------- ----------------- ------------ -----------------

\

**Example Response**

\

{

"success":true,

}

\

\

\

\

\

\

\

\

\

\

\

\

\

\

\

\

\

\

\

\

\

\

\

\

\

\

\

\

\

\

\

\

Common API’s {.western}
============

\

### Getting the list of all Information Pages {.western}

\

Retrieving the list of information pages

\

**API Endpoint**

**/application/pages**

\

**Request Method**

**GET**

\

**Parameters**

No Parameters required.

\

**Example Response**

\

{

success: true,

informations: [

{

id: "2",

title: "About us",

content: "\<p\>edd dskjcn\<strong\> dcsocm\<\\/strong\>\<\\/p\>\\ \\ ",

icon : "url"

},

]

}

\

### Getting a particular Information Page {.western}

\

Retrieving the a particular information page

\

**API Endpoint**

**/application/page/{id}******

\

**Request Method**

**GET**

\

**Parameters**

No Parameters required.

\

**Example Response**

\

{

success: true,

title: "About us",

content: "\<p\>edd dskjcn\<strong\> dcsocm\<\\/strong\>\<\\/p\>\\ \\ ",

icon : "url"

}

\

\

### Getting the list of all Types {.western}

\

Retrieving the list of all service types.

\

**API Endpoint**

**/application/types**

\

**Request Method**

**GET**

\

**Parameters**

No Parameters required.

\

**Example Response**

\

{

"success": true,

"types": [

{

"id": "1",

"name": "Benz",

"icon":
"https:\\/\\/wag-prabakaran.s3.amazonaws.com\\/14158620471615367883",

"is\_default": "1",

"price\_per\_unit\_time": "50.00",

"price\_per\_unit\_distance": "50.00",

"base\_price": "500.00"

}

]

}

\

\

\

\

### Forgot Password {.western}

This endpoint is used to reset both user’s and provider’s password.

\

**API Endpoint**

**/application/forgot-password**

\

**Request Method**

**POST**

\

**Parameters**

\

  -------------- ---------------- ------------ ------------------
  **Required**   **Parameters**   **Format**   **Explanation**

  **X**          **type**         **int**      **1 =\> Walker**
                                               
                                               **2 =\> User**

  **X**          **email**        **string**   \
                                               
  -------------- ---------------- ------------ ------------------

\

**Example Response**

\

{

"success":true,

}

56
